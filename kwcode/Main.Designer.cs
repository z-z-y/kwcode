﻿namespace kwcode
{
    partial class Kwcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Kwcode));
            this.stsBottom = new System.Windows.Forms.StatusStrip();
            this.tspbStatus = new System.Windows.Forms.ToolStripProgressBar();
            this.tsStatue = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsbtnHelp = new System.Windows.Forms.ToolStripSplitButton();
            this.tslblPower = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.clbTables = new System.Windows.Forms.CheckedListBox();
            this.grpConfig = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkPrefix = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.panCNName = new System.Windows.Forms.Panel();
            this.txtConnString = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnGetTables = new System.Windows.Forms.Button();
            this.btnTestConnect = new System.Windows.Forms.Button();
            this.btnCreateFiles = new System.Windows.Forms.Button();
            this.btnCreateCNName = new System.Windows.Forms.Button();
            this.stsBottom.SuspendLayout();
            this.grpConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // stsBottom
            // 
            this.stsBottom.BackColor = System.Drawing.Color.Transparent;
            this.stsBottom.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.stsBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspbStatus,
            this.tsStatue,
            this.tsbtnHelp,
            this.tslblPower});
            this.stsBottom.Location = new System.Drawing.Point(0, 497);
            this.stsBottom.Name = "stsBottom";
            this.stsBottom.Size = new System.Drawing.Size(878, 23);
            this.stsBottom.SizingGrip = false;
            this.stsBottom.Stretch = false;
            this.stsBottom.TabIndex = 18;
            this.stsBottom.Text = "stsPower";
            // 
            // tspbStatus
            // 
            this.tspbStatus.Margin = new System.Windows.Forms.Padding(13, 3, 1, 3);
            this.tspbStatus.Name = "tspbStatus";
            this.tspbStatus.Size = new System.Drawing.Size(100, 17);
            // 
            // tsStatue
            // 
            this.tsStatue.Margin = new System.Windows.Forms.Padding(13, 3, 1, 3);
            this.tsStatue.Name = "tsStatue";
            this.tsStatue.Size = new System.Drawing.Size(32, 17);
            this.tsStatue.Text = "状态";
            // 
            // tsbtnHelp
            // 
            this.tsbtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnHelp.DropDownButtonWidth = 0;
            this.tsbtnHelp.ForeColor = System.Drawing.Color.DarkRed;
            this.tsbtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnHelp.Image")));
            this.tsbtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnHelp.Margin = new System.Windows.Forms.Padding(420, 2, 0, 0);
            this.tsbtnHelp.Name = "tsbtnHelp";
            this.tsbtnHelp.Size = new System.Drawing.Size(57, 21);
            this.tsbtnHelp.Text = "   教程  ";
            this.tsbtnHelp.Click += new System.EventHandler(this.tsbtnHelp_Click);
            // 
            // tslblPower
            // 
            this.tslblPower.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tslblPower.Margin = new System.Windows.Forms.Padding(10, 3, 0, 2);
            this.tslblPower.Name = "tslblPower";
            this.tslblPower.Size = new System.Drawing.Size(227, 18);
            this.tslblPower.Text = "kwcode ver1.0  Powered by kerwincui";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMessage.Location = new System.Drawing.Point(202, 503);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(41, 12);
            this.lblMessage.TabIndex = 20;
            this.lblMessage.Text = "消息：";
            // 
            // clbTables
            // 
            this.clbTables.FormattingEnabled = true;
            this.clbTables.Location = new System.Drawing.Point(18, 24);
            this.clbTables.Name = "clbTables";
            this.clbTables.Size = new System.Drawing.Size(305, 452);
            this.clbTables.TabIndex = 27;
            this.clbTables.DoubleClick += new System.EventHandler(this.clbTables_DoubleClick);
            // 
            // grpConfig
            // 
            this.grpConfig.Controls.Add(this.label4);
            this.grpConfig.Controls.Add(this.label3);
            this.grpConfig.Controls.Add(this.txtPrefix);
            this.grpConfig.Controls.Add(this.label2);
            this.grpConfig.Controls.Add(this.chkPrefix);
            this.grpConfig.Controls.Add(this.label1);
            this.grpConfig.Controls.Add(this.cboType);
            this.grpConfig.Controls.Add(this.panCNName);
            this.grpConfig.Controls.Add(this.txtConnString);
            this.grpConfig.Controls.Add(this.label11);
            this.grpConfig.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.grpConfig.Location = new System.Drawing.Point(338, 24);
            this.grpConfig.Name = "grpConfig";
            this.grpConfig.Size = new System.Drawing.Size(521, 408);
            this.grpConfig.TabIndex = 25;
            this.grpConfig.TabStop = false;
            this.grpConfig.Text = "配置信息";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "设置表的中文名称";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(105, 219);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(400, 2);
            this.label3.TabIndex = 25;
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(255, 161);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(245, 23);
            this.txtPrefix.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(205, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "表前缀：";
            // 
            // chkPrefix
            // 
            this.chkPrefix.AutoSize = true;
            this.chkPrefix.Location = new System.Drawing.Point(93, 164);
            this.chkPrefix.Name = "chkPrefix";
            this.chkPrefix.Size = new System.Drawing.Size(87, 21);
            this.chkPrefix.TabIndex = 22;
            this.chkPrefix.Text = "去除表前缀";
            this.chkPrefix.UseVisualStyleBackColor = true;
            this.chkPrefix.CheckedChanged += new System.EventHandler(this.chkPrefix_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(11, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "数据库类型：";
            // 
            // cboType
            // 
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "SqlServer",
            "mysql"});
            this.cboType.Location = new System.Drawing.Point(92, 34);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(408, 25);
            this.cboType.TabIndex = 20;
            // 
            // panCNName
            // 
            this.panCNName.AutoScroll = true;
            this.panCNName.Location = new System.Drawing.Point(6, 239);
            this.panCNName.Name = "panCNName";
            this.panCNName.Size = new System.Drawing.Size(509, 164);
            this.panCNName.TabIndex = 19;
            // 
            // txtConnString
            // 
            this.txtConnString.Location = new System.Drawing.Point(92, 72);
            this.txtConnString.Multiline = true;
            this.txtConnString.Name = "txtConnString";
            this.txtConnString.Size = new System.Drawing.Size(408, 76);
            this.txtConnString.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(13, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 17);
            this.label11.TabIndex = 18;
            this.label11.Text = "连接字符串：";
            // 
            // btnGetTables
            // 
            this.btnGetTables.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnGetTables.Location = new System.Drawing.Point(438, 452);
            this.btnGetTables.Name = "btnGetTables";
            this.btnGetTables.Size = new System.Drawing.Size(101, 24);
            this.btnGetTables.TabIndex = 26;
            this.btnGetTables.Text = "获取表信息 >>";
            this.btnGetTables.UseVisualStyleBackColor = true;
            this.btnGetTables.Click += new System.EventHandler(this.btnGetTables_Click);
            // 
            // btnTestConnect
            // 
            this.btnTestConnect.BackColor = System.Drawing.SystemColors.Control;
            this.btnTestConnect.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnTestConnect.Location = new System.Drawing.Point(338, 452);
            this.btnTestConnect.Name = "btnTestConnect";
            this.btnTestConnect.Size = new System.Drawing.Size(88, 24);
            this.btnTestConnect.TabIndex = 24;
            this.btnTestConnect.Text = "测试连接 >>";
            this.btnTestConnect.UseVisualStyleBackColor = false;
            this.btnTestConnect.Click += new System.EventHandler(this.btnTestConnect_Click_1);
            // 
            // btnCreateFiles
            // 
            this.btnCreateFiles.Enabled = false;
            this.btnCreateFiles.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCreateFiles.Location = new System.Drawing.Point(661, 452);
            this.btnCreateFiles.Name = "btnCreateFiles";
            this.btnCreateFiles.Size = new System.Drawing.Size(198, 24);
            this.btnCreateFiles.TabIndex = 28;
            this.btnCreateFiles.Text = "生成文件";
            this.btnCreateFiles.UseVisualStyleBackColor = true;
            this.btnCreateFiles.Click += new System.EventHandler(this.btnCreateFiles_Click);
            // 
            // btnCreateCNName
            // 
            this.btnCreateCNName.Enabled = false;
            this.btnCreateCNName.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCreateCNName.Location = new System.Drawing.Point(549, 452);
            this.btnCreateCNName.Name = "btnCreateCNName";
            this.btnCreateCNName.Size = new System.Drawing.Size(101, 24);
            this.btnCreateCNName.TabIndex = 29;
            this.btnCreateCNName.Text = "设置中文名 >>";
            this.btnCreateCNName.UseVisualStyleBackColor = true;
            this.btnCreateCNName.Click += new System.EventHandler(this.btnCreateCNName_Click);
            // 
            // Kwcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(878, 520);
            this.Controls.Add(this.btnCreateCNName);
            this.Controls.Add(this.btnCreateFiles);
            this.Controls.Add(this.clbTables);
            this.Controls.Add(this.grpConfig);
            this.Controls.Add(this.btnGetTables);
            this.Controls.Add(this.btnTestConnect);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.stsBottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Kwcode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "kwcode";
            this.stsBottom.ResumeLayout(false);
            this.stsBottom.PerformLayout();
            this.grpConfig.ResumeLayout(false);
            this.grpConfig.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip stsBottom;
        private System.Windows.Forms.ToolStripStatusLabel tslblPower;
        private System.Windows.Forms.ToolStripProgressBar tspbStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsStatue;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.CheckedListBox clbTables;
        private System.Windows.Forms.GroupBox grpConfig;
        private System.Windows.Forms.TextBox txtConnString;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnGetTables;
        private System.Windows.Forms.Button btnTestConnect;
        private System.Windows.Forms.Button btnCreateFiles;
        private System.Windows.Forms.Button btnCreateCNName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkPrefix;
        private System.Windows.Forms.Panel panCNName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripSplitButton tsbtnHelp;
    }
}

